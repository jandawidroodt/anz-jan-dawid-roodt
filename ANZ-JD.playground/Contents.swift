import UIKit

// Jan-Dawid Roodt
// jandawidroodt@gmail.com
// https://www.linkedin.com/in/jdroodt/

//  - I made the assumption that the data would be used elsewhere in the code, so I made the functions
// return a tuple (gains, losses) to be used instead of just printing it out in the function.

//  - I also included error messages for inputs that are too small. ie. you can't subtract from only one
// number.

//  - I made the assumption that the input would be an array of Doubles but if we were reading it in from an
// outside source I would read it in as an array of Strings, turn them into Doubles, check that all the
// values were numerical and then continue as shown below.

// - I didn't assume that the share prices would be formatted with 2 decimal points so I did all my
// calculations without rounding until the very end (where it then rounds)

// - I assumed all share prices didn't include the currency CHAR (ie. '$') in front of the number.

// - I assumed all the share prices were in chronological order.

// - I assumed that the input array could theoretically be massive and therefore didn't use a recursive
// solution.



// Calculates the sum gains and sum losses from a given array of share prices.
//
// - Parameter: Array of share price values in chronological order. (2 or more values required)
// - Returns: Tuple value consisting of 2 single values representing sum gains and sum losses (Gains, Losses)
//
// Note: Could create a struct to represent gains and losses in a single object for transparency.
func calculateGainsAndLosses(_ sharePrices: [Double]) -> (Double, Double)? {
    
    var gainsTotal: Double = 0
    var lossesTotal: Double = 0
    
    // gains or losses can only be calculated when 2 or more values are given.
    if sharePrices.count <= 1 {
        print("Error: Cannot show gains or losses because array size needs to be 2 or more entries.")
        return nil
    }
    
    for (i, price) in sharePrices.enumerated() {
        if i > 0 {
            let diff = price - sharePrices[i-1]
            if diff > 0 {
                // Gain
                gainsTotal += diff
            } else {
                // Loss
                lossesTotal += diff
            }
        }
    }
    
    return (gainsTotal, lossesTotal)
}


// Usage:

let input: [Double] = [78.41, 85.18, 91.09, 90.574, 91.01, 103.61, 105.88, 103.77, 110.13, 108.89, 105.09]

if let (gains, losses) = calculateGainsAndLosses(input) {
    print(String(format: "%.2f", gains))
    print(String(format: "%.2f", losses))
} else {
    print("WARNING: Please provide more share price values.")
}

